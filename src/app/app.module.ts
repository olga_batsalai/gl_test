import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GlAccountsComponent} from './gl-accounts/components/gl-accounts/gl-accounts.component';
import {HttpClientModule} from "@angular/common/http";
import {GlCreateAccountPopupComponent} from './gl-accounts/components/gl-create-account-popup/gl-create-account-popup.component';


import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {
  NgbButtonsModule,
  NgbDatepickerModule,
  NgbModalModule,
  NgbModule,
  NgbNavModule
} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [
    AppComponent,
    GlAccountsComponent,
    GlCreateAccountPopupComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbButtonsModule,
    NgbNavModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
