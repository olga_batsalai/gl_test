import {Component, OnInit, Output} from '@angular/core';
import {GlAccountsService} from "../../services/gl-accounts.service";

@Component({
  selector: 'app-gl-accounts',
  templateUrl: './gl-accounts.component.html',
  styleUrls: ['./gl-accounts.component.scss']
})
export class GlAccountsComponent implements OnInit {

  constructor(public accountService: GlAccountsService) {
  }

  public accountList: Account[] = [];
  public columns: string[] = ['Name', 'Account name', 'Email', 'Status', 'Start date', 'Expiration date'];

  ngOnInit(): void {
    this.fetchAccounts();
    this.filterAccount();
  }

  fetchAccounts() {
    this.accountService.getAccounts()
      .subscribe(data => {
        console.log(this.accountList);
      });

  }

  compare(a, b) {
    let aDate = new Date(a[1]);
    let bDate = new Date(b[1]);
    if (aDate < bDate)
      return -1;
    if (aDate > bDate)
      return 1;
    return 0;
  }

  filterAccount() {
    let accArray = this.accountService.accountList;
    console.log('AccArray', accArray);
    let sortAccount = accArray.sort(this.compare);
    console.log(sortAccount);

  }
}

