import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlCreateAccountPopupComponent } from './gl-create-account-popup.component';

describe('GlCreateAccountPopupComponent', () => {
  let component: GlCreateAccountPopupComponent;
  let fixture: ComponentFixture<GlCreateAccountPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GlCreateAccountPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GlCreateAccountPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
