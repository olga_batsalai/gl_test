import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";
import {GlAccountsService} from "../../services/gl-accounts.service";

@Component({
  selector: 'app-gl-create-account-popup',
  templateUrl: './gl-create-account-popup.component.html',
  styleUrls: ['./gl-create-account-popup.component.scss']
})
export class GlCreateAccountPopupComponent implements OnInit {

  constructor(private modalService: NgbModal, private accountService: GlAccountsService) {
  }

  profileForm = new FormGroup({
    name: new FormControl('', [
      Validators.required
    ]),
    account: new FormControl('', [
      Validators.required
    ]),
    exampleSelect: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    startDate: new FormControl('', [
      Validators.required
    ]),
    expirationDate: new FormControl('', [
      Validators.required
    ])

  });

  closeResult = '';

  ngOnInit(): void {
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addNewAccount(): void {
    const account: any = {
      name: this.profileForm.value.name,
      account: this.profileForm.value.account,
      email: this.profileForm.value.email,
      exampleSelect: this.profileForm.value.exampleSelect,
      startDate: this.profileForm.value.startDate,
      expirationDate: this.profileForm.value.expirationDate
    };
    console.log('NewAccount', account);
    this.accountService.addNewAccount(account);
    this.profileForm.reset();
  }

}
