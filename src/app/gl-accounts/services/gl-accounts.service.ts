import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {tap} from "rxjs/operators";
import {Account} from "../shared/account";

@Injectable({providedIn: 'root'})
export class GlAccountsService {

  url: string = 'http://localhost:3000';
  public accountList: Account[] = [];

  constructor(private http: HttpClient) {
  }

  getAccounts() {
    return this.http.get<Account[]>(this.url + `/accounts`)
      .pipe(tap(data => {
        this.accountList = data;
      }))
  }

  addNewAccount(account: Account) {
    this.accountList.push(account);
  }
}
